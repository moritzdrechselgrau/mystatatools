*! Moritz Drechsel-Grau, 25 June 2020
*! building on mat2txt.ado by Ben Jann and M Blasnik (https://fmwww.bc.edu/repec/bocode/m/mat2txt.ado)
program define mat2csv

// Syntax
syntax using, matrix(name) [ replace append delim(string) format(str) nocolnames]

// Option Processing
if strlen("`delim'") > 1 & "`delim'" != "tab" {
	noi disp as error "delim has to be a single character in double quotes or tab"
}
else if "`delim'" == "" local delim = ","
else if "`delim'" == "tab" local delim = ""
else 

if "`delim'" == "" local tab _tab

if "`format'"=="" local format "%10.0g"
local formatn: word count `format'
tempname myfile

// number of columns and rows
local nrows = rowsof(`matrix')
local ncols = colsof(`matrix')

// Open file
file open `myfile' `using', write text `append' `replace'

// Header
if "`nocolnames'" == "" {
	QuotedFullnames `matrix' col // stores names in local colnames
	local i = 1
	foreach colname of local colnames {
		if `i' < `ncols' 	file write `myfile' `"`colname'"' "`delim'" `tab'
		else 							file write `myfile' `"`colname'"'
		local ++i
	}
}	
file write `myfile' _n

// Body
forvalues r=1/`nrows' {
	local i = 1
	forvalues c=1/`ncols' {
		if `c'<=`formatn' local fmt: word `c' of `format'
		local rc = string(`matrix'[`r',`c'], "`fmt'")
		if `i' < `ncols' 	file write `myfile' "`rc'" "`delim'" `tab'
		else							file write `myfile' "`rc'"
		local ++i
	}
	file write `myfile' _n
}

// Close file
file close `myfile'

end

program define QuotedFullnames
	args matrix type
	tempname extract
	local one 1
	local i one
	local j one
	if "`type'"=="row" local i k
	if "`type'"=="col" local j k
	local K = `type'sof(`matrix')
	forv k = 1/`K' {
		mat `extract' = `matrix'[``i''..``i'',``j''..``j'']
		local name: `type'names `extract'
		local eq: `type'eq `extract'
		if `"`eq'"'=="_" local eq
		else local eq `"`eq':"'
		local names `"`names'`"`eq'`name'"' "'
	}
	c_local `type'names `"`names'"'
end

/*
matrix M = matuniform(5,3)
matrix colnames M = "col1" "col 2" "col3"
mat2csv using "M.csv", matrix(M) replace delim(,) format(%9.3f)
*/