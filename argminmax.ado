cap program drop argminmax
program argminmax, rclass

	syntax varlist [if] [in] [aweight], x(string)
	
	if "`if'" == "" 	local ifpre = "if "
	else local ifpre = " & "
	
	foreach var of varlist `varlist' {
		qui  summarize `var' `if' `in' [`weight' `exp']
		local min = `r(min)'
		local max = `r(max)'
		qui sum `x' `if' `ifpre' `var' < `min'+ 1e-10 & `var' > `min'- 1e-10
		return local argmin_`var' = `r(mean)'
		return local nmin_`var' = `r(N)'
		qui sum `x' `if' `ifpre' `var' < `max'+ 1e-10 & `var' > `max'- 1e-10
		return local argmax_`var' = `r(mean)'
		return local nmax_`var' = `r(N)'
	}

end