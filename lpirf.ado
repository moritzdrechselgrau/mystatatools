cap program drop lpirf

program lpirf, eclass

	* SYNTAX
	* ----------------------------------------------------
	syntax varlist(min=2 fv ts) [if] [aweight], h(numlist) [diff alpha(real 0.05) wide printcontrols reghdfe(string) nograph twoway(string) pretty]
	
	* SETUP
	* ----------------------------------------------------
	tokenize `varlist'
	local yvar `1'
	local xvar `2'
	
	local controlvars 
	local i = 1
	foreach var in `varlist' {
		if `i' > 2 local controlvars `controlvars' `var'
		local ++i
	}
	
	* length of h
	local H = 0
	foreach k of numlist `h' {
		local ++H
	}

	* Matrices to store coefficients, standard errors and p-values
	matrix COEF = J(1,`H',0) // (H+1) x 1 matrix of zeros
	matrix SE = J(1,`H',0) // (H+1) x 1 matrix of zeros
	matrix PVAL = J(1,`H',0) // (H+1) x 1 matrix of zeros
	matrix OBS = J(1,`H',0) // (H+1) x 1 matrix of zeros
	
	* Check if data is tsset / xtset
	qui xtset
	local it = "`r(timevar)'"
	local ix = "`r(panelvar)'" 
	if ("`it'" == "") 	display as err "time variable not set"
	
	* Confidence Level
	local zcrit = invnormal(1-`alpha'/2)
	
	* CHOOSE THE SAMPLE
	marksample touse, novarlist
	markout `touse' `yvar' `xvar' `controlvars'
	

	* REGRESSIONS
	* ----------------------------------------------------
	ereturn clear
	eststo clear

	tempvar `yvar'_h _h_ _b_ _cilb_ _ciub_
	qui generate ``yvar'_h' = .
	qui generate `_h_' = .
	qui generate `_b_' = .
	qui generate `_cilb_' = .	
	qui generate `_ciub_' = .	
	
	quietly {
	local modelnames "h="
	
	local i = 1
	foreach k of numlist `h' {
		* outcome at horizon k
		replace ``yvar'_h' = .
		if ("`diff'" == "") replace ``yvar'_h' = f`k'.`yvar' - l.`yvar'
		else 								replace ``yvar'_h' = f`k'.`yvar'
		
		* cluster option
		if ("`cluster'" == "") 	local vce vce(robust)
		else 										local vce vce(cluster `cluster')
		
		* FE regression
		eststo, title("h=`k'"): reghdfe ``yvar'_h' `xvar' `controlvars' `if' [`weight' `exp'], `reghdfe' nosample

		* store the results
		matrix COEF[1,`i'] = _b[`xvar']
		matrix SE[1,`i'] = _se[`xvar']
		matrix PVAL[1,`i'] = 2 * (1-normal(abs(_b[`xvar']/_se[`xvar'])))
		matrix OBS[1,`i'] = `e(N)'
		
		* model names
		if `i' < `H' 	local modelnames "`modelnames'`k' h="
		else 					local modelnames "`modelnames'`k'"
		
		* auxiliary plotting variables
		replace `_h_' = `k' if _n == `i'
		replace `_b_' = _b[`xvar'] if _n == `i'
		replace `_cilb_' = _b[`xvar'] - `zcrit'*_se[`xvar'] if _n == `i'
		replace `_ciub_' = _b[`xvar'] + `zcrit'*_se[`xvar'] if _n == `i'
		
		* increment counter
		local ++i
	}
	}
	
	* GRAPH
	* ----------------------------------------------------
	if "`nograph'" == "" {
	
		* Variable labels
		local yvarlabel = strproper(subinstr("`yvar'", "_", " ", .))
		local xvarlabel = strproper(subinstr("`xvar'", "_", " ", .))
		
		* time unit
		qui xtset
		tokenize "`r(tdeltas)'"	 // e.g. "1 month"
		local tunit = strproper("`2's") //"month", for example	
		
		* option pretty: format of ylabels
		if "`pretty'" != ""	{
			qui prettyformat `_b_' if !missing(`_h_'), nticks(8) local(format)
			local ylabel ylabel(#8, format(`format'))
		}
		else local ylabel ylabel(#8)

		* towway options
		local options `ylabel' xlabel(`h') ytitle("Response of `yvarlabel'") ///
									xtitle("Periods since Impulse to `xvarlabel'") legend(off) ///
									`twoway'
		twoway	(line `_b_' `_h_' if !missing(`_h_'), lwidth(*1.5)) ///
						(rarea `_ciub_' `_cilb_' `_h_' if !missing(`_h_'), color(gs5%25)) ///
						, `options'
	}

	* E-CLASS OUTPUT
	* ----------------------------------------------------
	matrix colnames COEF = `modelnames'
	matrix colnames SE = `modelnames'
	matrix colnames PVAL = `modelnames'
	matrix colnames OBS = `modelnames'
	
	matrix rowjoinbyname RESULTS = COEF SE PVAL OBS
	matrix rownames RESULTS = "Coef" "Std Err" "p-value" "N"
	
	qui ereturn post COEF
	qui estadd matrix se = SE
	qui estadd matrix pvalue = PVAL
	qui estadd matrix N = OBS
	qui estadd matrix RESULTS = RESULTS'
	
	qui estadd scalar H = `H'

	* TABLE
	* ----------------------------------------------------
	if "`wide'" != "" {
		if "`printcontrols'" != "" 	local drop drop(_cons)
		else 												local drop drop(_cons `controlvars')
		esttab *, b se nonumbers `drop' mtitle title("Results of Local Projections")
	}
	else {
		esttab matrix(RESULTS, transpose fmt(%9.0g %9.0g %05.4f)), title("Results of Local Projections") 
	}
end

/*
qui cd "C:\Users\mdrech\HiDrive\users\moritz.drechsel-grau\SOFTWARE"
qui use "testdata.dta", clear
qui gen ldstir = l.dstir

lpirf dlgdpr ldstir l(1/3).dlgdpr l(1/3).dlcpi l(2/3).dstir cpi, h(0/8)
lpirf dlgdpr ldstir l(1/3).dlgdpr l(1/3).dlcpi l(2/3).dstir cpi, h(0/8) reghdfe(absorb(year)) diff //[diff alpha(real 0.05) wide printcontrols reghdfe(string) nograph twoway(string) pretty]
*/