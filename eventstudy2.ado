cap program drop eventstudy2

program eventstudy2, eclass

	* SYNTAX
	* ----------------------------------------------------
	syntax 	varlist(max=1 fv ts) [if] [aweight], ///
					events(varlist min=1) nlags(int) nleads(int) [nobins] ///
					[extrapolate_post extrapolate_pre absorbingdummy alpha(real 0.05) reghdfe(string) twoway(string) pretty keepvars verbose]
	
	capture noisily {
	quietly {
	
    if "`verbose'" != "" local noisily = "noisily"
  
		* SETUP
		* ----------------------------------------------------
		
		// Temporary variables
		tempvar invtimevar _sum
		
		// outcome and event variable
		tokenize `varlist'
		local yvar `1'	// first variable must be outcome variable
		
		// other controls
		local controlvars 
		local i = 1
		foreach var in `varlist' {
			if `i' > 2 local controlvars `controlvars' `var'
			local ++i
		}
    
		// Length of range from maxlead to maxlag
		local nrows = `nleads' + `nlags' + 1
		local nescoef = `nleads' + `nlags'
		
		// Check if data is tsset / xtset
		qui xtset
		local timevar = "`r(timevar)'"
		local panelvar = "`r(panelvar)'" 
		if ("`timevar'" == "") | ("`panelvar'" == "")	{
			noi display as err "data not properly xtset"
			exit
		}
    
    // Loop over events
    foreach eventvar in `events' {
    
      qui sum `timevar' if !missing(`eventvar')
      local tmin = `r(min)'
      local tmax = `r(max)'
      local max_lags = `tmax' - `tmin' - `nleads' + 1 
      local max_leads = `tmax' - `tmin' - `nlags' + 1

      
      // Construct lags of event variable
      if `nlags' >= 0 {
      foreach k of numlist 0/`nlags' {
        cap drop L`k'_`eventvar'
        generate L`k'_`eventvar' = L`k'.`eventvar'
        if "`extrapolate_post'" != "" replace L`k'_`eventvar' = 0 if `timevar'-`k' < `tmin'
        label variable L`k'_`eventvar' "L`k'.`eventvar'"
        // Correct ends of event window
        if (`k' == `nlags') & "`nobins'" == "" {
          foreach kbin of numlist `=`nlags'+1'/`max_lags' {
            replace L`k'_`eventvar' = L`k'_`eventvar' + L`kbin'.`eventvar' if `timevar'-`kbin' >= `tmin'
          }
          if "`absorbingdummy'" != "" {
            replace L`k'_`eventvar' = L`k'_`eventvar' > 0 if !missing(L`k'_`eventvar')
          }
        }
      }
      }
      
      // Construct leads of event variable
      if `nleads' >= 2 {
      foreach k of numlist `nleads'(-1)2  {		 
        cap drop F`k'_`eventvar'
        generate F`k'_`eventvar' = F`k'.`eventvar'
        if "`extrapolate_pre'" != "" replace F`k'_`eventvar' = 0 if `timevar'+`k' > `tmax'
        label variable F`k'_`eventvar' "F`k'.`eventvar'"
        // Correct ends of event window
        if (`k' == `nleads') & "`nobins'" == ""  {
          foreach kbin of numlist `=`nleads'+1'/`max_leads' {
            replace F`k'_`eventvar' = F`k'_`eventvar' + F`kbin'.`eventvar' if `timevar'+`kbin' <= `tmax'
          }
          if "`absorbingdummy'" != "" {
            replace F`k'_`eventvar' = F`k'_`eventvar' > 0 if !missing(F`k'_`eventvar')
          }
        }
      }
      }
      sort `panelvar' `timevar'
      
    } // evar
    

		
		// Collect event variables
		// ----------------------------------------------------
		local eventvars = ""
		
    foreach eventvar in `events' {
      // leads
      if `nleads' >= 2 {
      foreach k of numlist `nleads'(-1)2 {
        local eventvars = "`eventvars' F`k'_`eventvar' "
      }
      }
      // lags
      if `nlags' >= 0 {
      foreach k of numlist 0/`nlags' {
        local eventvars = "`eventvars' L`k'_`eventvar' "
      }
      }
      
    }

		// mark sample
		marksample touse, novarlist
		markout `touse' `yvar' `eventvars' `controlvars'
		
		// REGRESSION
		// ----------------------------------------------------
		
		// reghdfe options
		if "`reghdfe'" == "" local reghdfe = "noabsorb"
		// regression
		ereturn clear
		tempname regoutput
		`noisily' reghdfe `yvar' `eventvars' `controlvars' `if' [`weight' `exp'], `reghdfe'
		estimates store `regoutput'
		ereturn post
		

			
		// Coefficients, Confidence Bands, etc.
		// ----------------------------------------------------
		estimates restore `regoutput'
    
		foreach eventvar in `events' {
	
      // Auxiliary plotting variables
      tempvar _k_`eventvar' _b_`eventvar' _se_`eventvar' _cilb_`eventvar' _ciub_`eventvar'
      generate `_k_`eventvar'' = _n - `nleads' - 1 in 1/`nrows' 
      generate `_b_`eventvar'' = 0 in 1/`nrows'
      generate `_se_`eventvar'' = 0 in 1/`nrows'
      generate `_cilb_`eventvar'' = 0 in 1/`nrows'
      generate `_ciub_`eventvar'' = 0 in 1/`nrows'
      
      local zcrit = invnormal(1-`alpha'/2)
      
      // time relative to event
      local i = 1
      foreach k of numlist -`nleads'/`nlags' {
        replace `_k_`eventvar'' = `k' if _n == `i'
        local ++i
      }
      // Coefficients and CI for lags
      if `nlags' >= 0 {
      foreach k of numlist 0/`nlags' {
        replace `_b_`eventvar'' = _b[L`k'_`eventvar'] if `_k_`eventvar'' == `k'
        replace `_se_`eventvar'' = _se[L`k'_`eventvar'] if `_k_`eventvar'' == `k'
        replace `_cilb_`eventvar'' = _b[L`k'_`eventvar'] - `zcrit' * _se[L`k'_`eventvar'] if `_k_`eventvar'' == `k'
        replace `_ciub_`eventvar'' = _b[L`k'_`eventvar'] + `zcrit' * _se[L`k'_`eventvar'] if `_k_`eventvar'' == `k'
      }
      }
      // Coefficients and CI for leads
      if `nleads' >= 2 {
      foreach k of numlist 2/`nleads' {
        replace `_b_`eventvar'' = _b[F`k'_`eventvar'] if `_k_`eventvar'' == -`k'
        replace `_se_`eventvar'' = _se[F`k'_`eventvar'] if `_k_`eventvar'' == -`k'
        replace `_cilb_`eventvar'' = _b[F`k'_`eventvar'] - `zcrit' * _se[F`k'_`eventvar'] if `_k_`eventvar'' == -`k'
        replace `_ciub_`eventvar'' = _b[F`k'_`eventvar'] + `zcrit' * _se[F`k'_`eventvar'] if `_k_`eventvar'' == -`k'
      }
      }
      
      // Matrix to store results of event study coefficients
      matrix RESULTS = J(`nrows',5,0) 	// nrows x 5 matrix of zeros
      matrix colnames RESULTS = "k" "Coef" "SE" "CIlow" "CIup"
      foreach r of numlist 1/`nrows' {
        matrix RESULTS[`r',1] = `_k_`eventvar''[`r']
        matrix RESULTS[`r',2] = `_b_`eventvar''[`r']
        matrix RESULTS[`r',3] = `_se_`eventvar''[`r']
        matrix RESULTS[`r',4] = `_cilb_`eventvar''[`r']
        matrix RESULTS[`r',5] = `_ciub_`eventvar''[`r']
      }
      estadd matrix RESULTS_`eventvar' = RESULTS
      
    }
		
		// Variance-covariance matrix of event study coefficients (for aggregation)
		/* WORK IN PROGRESS !!! 
		matrix V_es = J(`nrows',`nrows',0) 

		foreach k1 of numlist `invleads' {
			
		}
		*/
		
		
		// Graph
    foreach eventvar in `events' {
      twoway	(connect `_b_`eventvar'' `_k_`eventvar'' if !missing(`_k_`eventvar''), color(gs5) lwidth(*1.5)) ///
              (rcap `_ciub_`eventvar'' `_cilb_`eventvar'' `_k_`eventvar'' if !missing(`_k_`eventvar''), color(gs5)) ///
              , ///
              xlabel(-`nleads'/`nlags') ytitle("") ///
              xtitle("Periods Relative to Event") legend(off) ///
              name(`eventvar', replace) title("Event Study: `eventvar'") nodraw ///
              `twoway'
    }
    graph combine `events', ycommon
	
		// Drop leads and lags
		if "`keepvars'" == "" {
      foreach eventvar in `events' {
        if `nlags' >= 0 	drop L*_`eventvar'
        if `nleads' >= 2 	drop F*_`eventvar'
      }
		}

	} // quietly
	} // capture noisily
	// Clean up if error	
	if _rc != 0 & "`keepvars'" == "" {
    foreach eventvar in `events' {
      cap drop L*_`eventvar'
      cap drop F*_`eventvar'
    }
		exit
	}

	
end

/*
use id_lor id_bezreg id_bezirk name_lor break_size break_quarter target period pricevar ///
	using "data/structbreaks" ///
	if target == "lor2018" & period == "quarter" & pricevar == "i24_nrent_r", clear
	
drop target period pricevar
merge 1:m id_lor using "data/pricepanel_quarter_lor2018", assert(3) nogen keepusing(i24_nrent_r quarter)
xtset id_lor quarter
sort id_lor quarter

gen event  = (quarter == break_quarter)
gen eventsize = event * break_size * 100
gen log_rent = log(i24_nrent_r)


eststo testest, noesample: eventstudy log_rent event, lags(0/8) leads(2/8) ///
	reghdfe(absorb(i.id_lor) noconstant cluster(id_lor)) ///
	twoway(name(es1, replace)) 
esttab testest, se wide
	
eventstudy log_rent eventsize, reghdfe(absorb(i.id_lor) noconstant cluster(id_lor)) lags(8) leads(8) ///
	twoway(name(es2, replace)) pretty

	
	
*/
