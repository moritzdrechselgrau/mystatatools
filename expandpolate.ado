cap program drop expandpolate
program expandpolate

	syntax varlist , xvar(string) [step(real 0.1) by(string) linear]
	
	
	capture noisily {
	quietly {
	
	tempfile temp1 temp2 temp3
	
	summarize `xvar'
	local x_min = `r(min)'
	local x_max = `r(max)'
	local x_range = `x_max' - `x_min'
	local n = ceil(`x_range' / `step') + 1	
	
	if "`by'" != "" {
		levelsof `by'
		local levels "`r(levels)'"
		if substr("`:type `by''" , 1, 3) == "str" {
			local bystring = 1					
		}
		else {
			local bystring = 0
		}
	}
	
	save "`temp1'", replace	
		
	if "`by'" != "" {
		noi disp as text "Expand & Interpolate by `by'"
		clear
		foreach k of local levels {
			noi disp as text "`by'=`k', " _c
			preserve
				if `bystring' == 1 	use if `by' == "`k'" using "`temp1'", clear
				else 								use if `by' == `k' using "`temp1'", clear										
				save "`temp2'", replace
				keep if _n == 1
				expand `n'
				replace `xvar' =  `x_min' + (_n-1) * `step'
				drop if `xvar' > `x_max'
				sort `xvar'
				
				drop `varlist'	
				merge 1:1 `by' `xvar' using "`temp2'", keep(master match) nogenerate
				foreach var in `varlist' {
          disp `var'
          
          if "`linear'" == "" {
            qui cipolate `var' `xvar', generate(_`var')
            qui replace `var' = _`var' if missing(`var')
            drop _`var'
          }
          
					qui ipolate `var' `xvar', generate(_`var')
					qui replace `var' = _`var' if missing(`var')
					drop _`var'
				}

				//replace `xvar' = round(`xvar' / `step') * `step'
				sort `xvar'
				save "`temp3'", replace 
			restore			
			append using "`temp3'"
		}
		noi disp as text "complete."
	}
	else {
		noi disp as text "Expand & Interpolate"
		keep if _n == 1
		expand `n'
		replace `xvar' =  `x_min' + (_n-1) * `step'
		drop if `xvar' > `x_max'
		sort `xvar'

		drop `varlist'	
		merge 1:1 `xvar' using "`temp1'", keep(master match) nogenerate
		foreach var in `varlist' {
			disp `var'
      if "`linear'" == "" {
        qui cipolate `var' `xvar', generate(_`var')
        qui replace `var' = _`var' if missing(`var')
        drop _`var'
      }
			qui ipolate `var' `xvar', generate(_`var')
			qui replace `var' = _`var' if missing(`var')
			drop _`var'
		}

		//replace `xvar' = round(`xvar' / `step') * `step'
		sort `xvar'
	}
	
		
	} // capture noisily
	} // quietly
	// Clean up if error	
	if _rc != 0 {
		use "`temp1'", clear	
		exit
	}


end